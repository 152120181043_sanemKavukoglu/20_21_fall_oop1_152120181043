#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
vector<int> parseInts(string str)
{
	vector<int> temp_arr;
	stringstream ss(str);
	int temp;char ch;
	while (ss >> temp)
	{
		temp_arr.push_back(temp);
		ss >> ch;
	}
	return temp_arr;
}
int main()
{
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++)
	{
		cout << integers[i] << "\n";
	}
	system("pause");
}
