#include <iostream>
#include <cstdio>
using namespace std;

int max_of_four(int a, int b, int c, int d)
{
	for (int i = 0;i < 4;i++)
	{
		int max, max2;
		if (a < b)
			max = b;
		else
			max = a;
		if (c < d)
			max2 = d;
		else
			max2 = c;
		if (max < max2)
			return max2;
		else
			return max;
	}
	return 0;
}

int main() {
	int a, b, c, d;
	scanf("%d %d %d %d", &a, &b, &c, &d);
	int ans = max_of_four(a, b, c, d);
	printf("%d", ans);

	system("pause");
}
