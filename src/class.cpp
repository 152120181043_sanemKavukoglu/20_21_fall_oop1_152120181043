#include <iostream>
#include <sstream>
using namespace std;

class Student
{
private:
	int agee, stndrt;
	string namee, lnamee, ss;

public:
	void set_age(int age)
	{
		agee = age;
	}
	int get_age()
	{
		return agee;
	}
	void set_first_name(string first_name)
	{
		namee = first_name;
	}
	string get_first_name()
	{
		return namee;
	}
	void set_last_name(string last_name)
	{
		lnamee = last_name;
	}
	string get_last_name()
	{
		return lnamee;
	}
	void set_standard(int standard)
	{
		stndrt = standard;
	}
	int get_standard()
	{
		return stndrt;
	}
	string to_string()
	{
		cout << agee << "," << namee << "," << lnamee << "," << stndrt;
		return " ";
	}
};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	system("pause");
}
